const axios = require('axios');

(async () => {
  const { data } = await axios.post('http://127.0.0.1:3000/auth/login', {
    username: 'username',
    password: 'password',
  });

  try {
    const { data: res } = await axios.get('http://127.0.0.1:3000/auth', {
      headers: { authorization: `Bearer ${data.token}` },
    });

    console.log(res); // Add logger
  } catch ({ response: { data } }) {
    console.log(data);
  }
})();
