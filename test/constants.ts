import 'dotenv/config';

export const globalPrefix = '/api/v1';
export const app = `http://127.0.0.1:${process.env.PORT}${globalPrefix}`;
