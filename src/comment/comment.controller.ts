import { Controller, Post, UseGuards, Param, Body, Req } from '@nestjs/common';
import { CommentService } from './comment.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../utilities/user.decorator';
import { IUser } from '../types/user';
import { ICreateCommentDTO } from './comment.dto';

@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) {}

  @Post(':postId')
  @UseGuards(AuthGuard('jwt'))
  async addComment(
    @Param('postId') postId: string,
    @Body() createCommentDTO: ICreateCommentDTO,
    @User() user: IUser,
  ) {
    return await this.commentService.add(postId, user._id, createCommentDTO);
  }
}
