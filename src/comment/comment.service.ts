import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IComment } from '../types/comment';
import { ICreateCommentDTO } from './comment.dto';
import { IPost } from '../types/post';

@Injectable()
export class CommentService {
  constructor(
    @InjectModel('Comment') private readonly commentModel: Model<IComment>,
    @InjectModel('Post') private readonly postModel: Model<IPost>,
  ) {}

  async add(
    postId: string,
    userId: string,
    createCommentDTO: ICreateCommentDTO,
  ) {
    if (!createCommentDTO.data.text) {
      throw new HttpException(
        'Not valid comment payload',
        HttpStatus.BAD_REQUEST,
      );
    }
    const comment = await this.commentModel.create({
      ...createCommentDTO,
      author: userId,
      post: postId,
    });
    await this.postModel.findByIdAndUpdate(postId, {
      $push: { comments: comment._id },
    });
    return comment;
  }
}
