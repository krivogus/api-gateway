export interface ICreateCommentDTO {
  data: {
    text: string;
  };
}
