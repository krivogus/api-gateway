import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CommentSchema } from '../models/comment.schema';
import { CommentService } from './comment.service';
import { CommentController } from './comment.controller';
import { PostModule } from 'src/post/post.module';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Comment', schema: CommentSchema }]),
    MongooseModule.forFeature([{ name: 'Post', schema: PostModule }]),
  ],
  providers: [CommentService],
  controllers: [CommentController],
})
export class CommentModule {}
