import { Controller, Post, Body, Get, UseGuards } from '@nestjs/common';
import { UserService } from '../shared/user.service';
import { ILoginDTO, IRegisterDTO } from './auth.dto';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { IPayload } from '../types/payload';
import { IUser } from '../types/user';
import { User } from '../utilities/user.decorator';

@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  async login(@Body() userDTO: ILoginDTO) {
    const user = await this.userService.findByLogin(userDTO);
    const payload: IPayload = { username: user.username };
    const token = await this.authService.signPayload(payload);
    return { user, token };
  }

  @Post('register')
  async register(@Body() userDTO: IRegisterDTO) {
    const user: IUser = await this.userService.create(userDTO);
    const payload: IPayload = { username: user.username };
    const token = await this.authService.signPayload(payload);
    return { user, token };
  }

  @Get('user')
  @UseGuards(AuthGuard('jwt'))
  async user(@User() user: IUser) {
    return { user };
  }
}
