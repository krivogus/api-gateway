import * as Joi from '@hapi/joi';
import * as mongoose from 'mongoose';

export const validateFindPostsCondition = async value =>
  Joi.object({
    author: Joi.string().custom(author => {
      if (!mongoose.Types.ObjectId.isValid(author)) {
        throw new Error('Invalid author id');
      }
      return author;
    }),
    ...['data.text', 'data.photo'].reduce(
      (acc, key) => ({ ...acc, [key]: Joi.object({ $exists: true }) }),
      {},
    ),
  }).validateAsync(value);
