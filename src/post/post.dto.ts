import { HttpStatus } from '@nestjs/common';
import { IFileDTO } from 'src/types/file';

export interface IPostData {
  data: {
    text?: string;
    photo?: string;
  };
}

export interface IGetPostsDTO {
  filter?: {
    author?: string;
    dataType?: 'text' | 'photo';
  };
  sortBy?: 'likes' | 'createdAt';
  pagination?: {
    skip?: number;
    limit?: number;
  };
}

export interface ISendFileDTO extends IFileDTO {
  fileId: string;
}

export interface IResponseDTO {
  readonly statusCode: HttpStatus;
  readonly message: string;
}

export interface IGetFileDTO {
  readonly fileId: string;
}

export interface ICreatePostDTO {
  readonly text: string;
}
