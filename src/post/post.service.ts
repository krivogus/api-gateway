import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IPost } from '../types/post';
import { IPostData, IGetPostsDTO } from './post.dto';
import { ILikes } from '../types/likes';
import { IUser } from '../types/user';
import { validateFindPostsCondition } from './post.validator';
@Injectable()
export class PostService {
  constructor(
    @InjectModel('Post') private readonly postModel: Model<IPost>,
    @InjectModel('Likes') private readonly likeModel: Model<ILikes>,
    @InjectModel('User') private readonly userModel: Model<IUser>,
  ) {}

  async findAll(
    getPostsDTO: IGetPostsDTO = { pagination: { skip: 0, limit: 20 } },
  ): Promise<IPost[]> {
    const condition: any = {};
    const { filter, sortBy, pagination } = getPostsDTO;

    if (filter && filter.author) {
      condition.author = getPostsDTO.filter.author;
    }
    if (filter && filter.dataType) {
      condition[`data.${filter.dataType}`] = { $exists: true };
    }

    try {
      await validateFindPostsCondition(condition);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }

    const query = this.postModel.find(condition, null, pagination);

    if (sortBy) {
      query.sort({ [sortBy]: -1 });
    }

    return await query;
  }

  async findOne(id: string): Promise<IPost> {
    return await this.postModel.findById(id).select('-__v');
  }

  async create(postDTO: IPostData, authorId: string) {
    const [post, user] = await Promise.all([
      this.postModel.create(postDTO),
      this.userModel.findById(authorId),
    ]);

    const likes = await this.likeModel.create({ post: post._id });

    post.author = user._id;
    likes.post = post._id;
    user.posts.push(post._id);
    post.likes = likes._id;

    await Promise.all([post.save(), user.save(), likes.save()]);

    return post;
  }

  async delete(id: string): Promise<IPost> {
    const post = await this.postModel.findById(id);
    const user = await this.userModel.findByIdAndUpdate(post.author, {
      $pull: { posts: id },
    });

    await post.remove();
    await user.save();

    return post;
  }
}
