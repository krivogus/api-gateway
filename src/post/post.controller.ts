import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  UseGuards,
  Body,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import * as uuid from 'uuid';
import { PostService } from './post.service';
import {
  IGetPostsDTO,
  ICreatePostDTO,
  IResponseDTO,
  ISendFileDTO,
} from './post.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../utilities/user.decorator';
import { IUser } from '../types/user';
import { ClientProxyFactory, ClientProxy } from '@nestjs/microservices';
import { microserviceOptions } from '../microservice/microservice.config';
import { FileInterceptor } from '@nestjs/platform-express';
import { IFileDTO } from 'src/types/file';

@Controller('post')
export class PostController {
  private readonly client: ClientProxy;

  constructor(private readonly postService: PostService) {
    this.client = ClientProxyFactory.create(microserviceOptions);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'))
  async listAll(@Body() getPostsDTO?: IGetPostsDTO) {
    return await this.postService.findAll(getPostsDTO);
  }

  @Post()
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileInterceptor('photo'))
  async sendFile(
    @UploadedFile() file: IFileDTO,
    @User() user: IUser,
    @Body() { text }: ICreatePostDTO,
  ) {
    if (!file) {
      return await this.postService.create({ data: { text } }, user._id);
    }
    const fileId = `${uuid.v4()}__${file.originalname}`;
    const res = await this.client
      .send<IResponseDTO, ISendFileDTO>({ file: 'post' }, { ...file, fileId })
      .toPromise();
    if (res.statusCode < 200 || res.statusCode >= 300) {
      throw new Error();
    }
    return await this.postService.create(
      { data: { photo: fileId, text } },
      user._id,
    );
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async read(@Param('id') id: string) {
    return await this.postService.findOne(id);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async delete(@Param('id') id: string) {
    return await this.postService.delete(id);
  }
}
