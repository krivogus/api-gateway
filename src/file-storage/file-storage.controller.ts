import {
  Controller,
  Post,
  Param,
  UseInterceptors,
  UploadedFile,
  Get,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { FileStorageService } from './file-storage.service';
import { ClientProxy } from '@nestjs/microservices';
import { FileInterceptor } from '@nestjs/platform-express';
import { ClientProxyFactory } from '@nestjs/microservices';
import { IResponseDTO, IGetFileDTO, ISendFileDTO } from './file-storage.dto';
import { Response } from 'express';
import { IFileDTO } from 'src/types/file';
import { microserviceOptions } from 'src/microservice/microservice.config';

@Controller('file-storage')
export class FileStorageController {
  private readonly client: ClientProxy;

  constructor(private readonly fileStorageService: FileStorageService) {
    this.client = ClientProxyFactory.create(microserviceOptions);
  }

  @Post(':fileId')
  @UseInterceptors(FileInterceptor('file'))
  sendFile(@UploadedFile() file: IFileDTO, @Param('fileId') fileId: string) {
    return this.client.send<IResponseDTO, ISendFileDTO>(
      { file: 'post' },
      { ...file, fileId },
    );
  }

  @Get(':fileId')
  async getFile(
    @Param('fileId') fileId: string,
    @Res() res: Response,
  ): Promise<void> {
    const file = await this.client
      .send<any, IGetFileDTO>({ file: 'get' }, { fileId })
      .toPromise();
    return res
      .status(HttpStatus.OK)
      .contentType('image/png')
      .end(file, 'binary');
  }
}
