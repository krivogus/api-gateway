import { Transport, ClientOptions } from '@nestjs/microservices';

export const microserviceOptions: ClientOptions = {
  transport: Transport.TCP,
  options: {
    host: process.env.API_GATEWAY_HOST,
    port: Number(process.env.API_GATEWAY_PORT),
  },
};
