import {
  Controller,
  Patch,
  UseGuards,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ProfileService } from './profile.service';
import { AuthGuard } from '@nestjs/passport';
import { IFileDTO } from '../types/file';
import { FileInterceptor } from '@nestjs/platform-express';
import * as uuid from 'uuid';
import { Transport } from '@nestjs/common/enums/transport.enum';
import {
  ClientProxyFactory,
  ClientProxy,
  ClientOptions,
} from '@nestjs/microservices';
import { IResponseDTO, ISendFileDTO } from 'src/file-storage/file-storage.dto';
import { User } from 'src/utilities/user.decorator';
import { IUser } from 'src/types/user';

const microserviceOptions: ClientOptions = {
  transport: Transport.TCP,
  options: {
    host: process.env.API_GATEWAY_HOST,
    port: Number(process.env.API_GATEWAY_PORT),
  },
};

@Controller('profile')
export class ProfileController {
  private readonly client: ClientProxy;

  constructor(private readonly profileService: ProfileService) {
    this.client = ClientProxyFactory.create(microserviceOptions);
  }

  @Patch('avatar')
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileInterceptor('file'))
  async setAvatar(@UploadedFile() file: IFileDTO, @User() user: IUser) {
    const fileId = `${uuid.v4()}__${file.originalname}`;
    const res = await this.client
      .send<IResponseDTO, ISendFileDTO>({ file: 'post' }, { ...file, fileId })
      .toPromise();
    if (res.statusCode < 200 || res.statusCode >= 300) {
      throw new Error();
    }
    await this.profileService.addAvatarId(fileId, user._id);
    return { ...res, fileId };
  }
}
