import { HttpStatus } from '@nestjs/common';
import { IFileDTO } from 'src/types/file';

export interface ISendFileDTO extends IFileDTO {
  fileId: string;
}

export interface IResponseDTO {
  readonly statusCode: HttpStatus;
  readonly message: string;
}

export interface IGetFileDTO {
  readonly fileId: string;
}
