import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/types/user';
import { IProfile } from 'src/types/profile';

@Injectable()
export class ProfileService {
  constructor(
    @InjectModel('User') private readonly userModel: Model<IUser>,
    @InjectModel('Profile') private readonly profileModel: Model<IProfile>,
  ) {}

  async addAvatarId(avatarId: string, userId: string) {
    const profile = await this.profileModel.findOneAndUpdate(
      { user: userId },
      { avatarSrc: avatarId },
      { new: true },
    );
    return profile;
  }
}
