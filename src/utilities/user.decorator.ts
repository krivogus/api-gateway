import { createParamDecorator } from '@nestjs/common';
import { IUser } from 'src/types/user';

export const User = createParamDecorator((data, req) => req.user as IUser);
