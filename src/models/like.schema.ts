import * as mongoose from 'mongoose';
import { ILikes } from 'src/types/likes';

export const LikesSchema = new mongoose.Schema(
  {
    post: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Post',
    },
    amount: {
      type: Number,
      default: 0,
    },
    users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
  },
  {
    timestamps: true,
  },
);

const selectUser = {
  select: 'username',
  populate: { path: 'profile', select: 'avatarSrc' },
};

const populationOptions:
  | mongoose.ModelPopulateOptions
  | mongoose.ModelPopulateOptions[] = [
  {
    path: 'users',
    ...selectUser,
  },
  { path: 'post', select: '-comments -author -likes -data' },
];

LikesSchema.pre<ILikes>(/find.*/, function() {
  this.populate(populationOptions);
});

LikesSchema.post<ILikes>('save', (doc, next) => {
  doc
    .populate(populationOptions)
    .execPopulate()
    .then(() => {
      next();
    });
});
