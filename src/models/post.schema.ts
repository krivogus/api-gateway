import * as mongoose from 'mongoose';
import { IPost } from 'src/types/post';

export const PostSchema = new mongoose.Schema(
  {
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    data: {
      text: {
        required: false,
        type: String,
      },
      photo: {
        required: false,
        type: String,
      },
    },
    comments: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment',
      },
    ],
    likes: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Likes',
    },
  },
  {
    timestamps: true,
  },
);

const selectUser = {
  select: 'username',
  populate: { path: 'profile', select: 'avatarSrc' },
};

const populationOptions:
  | mongoose.ModelPopulateOptions
  | mongoose.ModelPopulateOptions[] = [
  {
    path: 'author',
    ...selectUser,
  },
  {
    path: 'likes',
    select: 'amount',
    populate: {
      path: 'users',
      ...selectUser,
    },
  },
  {
    path: 'comments',
    select: '-post',
    populate: {
      path: 'author',
      ...selectUser,
    },
  },
];

PostSchema.pre<IPost>(/find.*/, function() {
  this.populate(populationOptions);
});

PostSchema.post<IPost>('save', function(doc, next) {
  doc
    .populate(populationOptions)
    .execPopulate()
    .then(() => {
      next();
    });
});
