import * as mongoose from 'mongoose';

export const ProfileSchema = new mongoose.Schema(
  {
    avatarSrc: String,
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    timestamps: true,
  },
);
