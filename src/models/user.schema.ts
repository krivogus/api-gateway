import * as mongoose from 'mongoose';
import * as bcrypt from 'bcrypt';
import { IUser } from '../types/user';

export const UserSchema = new mongoose.Schema(
  {
    profile: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Profile',
    },
    username: String,
    password: {
      type: String,
      select: false,
    },
    firstName: String,
    lastName: String,
    status: String,
    friends: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
    ],
    posts: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post',
      },
    ],
    address: {
      city: String,
      country: String,
    },
  },
  {
    timestamps: true,
  },
);

const populationOptions = [
  {
    path: 'friends',
    populate: { path: 'profile' },
  },
  {
    path: 'profile',
    select: 'avatarSrc',
  },
  {
    path: 'posts',
    options: {
      sort: { createdAt: -1 },
    },
    populate: [
      { path: 'author', select: 'username' },
      { path: 'likes', select: 'amount' },
      {
        path: 'comments',
        select: '-post',
        populate: { path: 'author', select: 'username' },
      },
    ],
  },
];

UserSchema.pre<IUser>('find', function() {
  this.populate({
    path: 'profile',
    select: 'avatarSrc',
  });
});

UserSchema.pre<IUser>(/find.+/, function() {
  this.populate(populationOptions);
});

UserSchema.pre<IUser>('save', async function(next: mongoose.HookNextFunction) {
  try {
    if (!this.isModified('password')) {
      return next();
    }
    const hashed = await bcrypt.hash(this.password, 10);
    // TODO: make readonly type
    this.password = hashed;
    return next();
  } catch (err) {
    return next(err);
  }
});
