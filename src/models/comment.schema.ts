import * as mongoose from 'mongoose';
import { IComment } from 'src/types/comment';

export const CommentSchema = new mongoose.Schema(
  {
    data: {
      text: String,
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
    },
    post: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Post',
    },
  },
  {
    timestamps: true,
  },
);

const selectUser = {
  select: 'username',
  populate: { path: 'profile', select: 'avatarSrc' },
};

const populationOptions:
  | mongoose.ModelPopulateOptions
  | mongoose.ModelPopulateOptions[] = [
  {
    path: 'author',
    ...selectUser,
  },
  { path: 'post', select: '-comments -author -likes -data' },
];

CommentSchema.pre<IComment>(/find.*/, function() {
  this.populate(populationOptions);
});

CommentSchema.post<IComment>('save', (doc, next) => {
  doc
    .populate(populationOptions)
    .execPopulate()
    .then(() => {
      next();
    });
});
