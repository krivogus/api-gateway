import { Module } from '@nestjs/common';
import { LikesService } from './likes.service';
import { LikesController } from './likes.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { LikesSchema } from 'src/models/like.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Likes', schema: LikesSchema }]),
  ],
  providers: [LikesService],
  controllers: [LikesController],
})
export class LikesModule {}
