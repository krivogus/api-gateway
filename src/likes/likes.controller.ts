import { Controller, Post, Delete, UseGuards, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LikesService } from './likes.service';
import { User } from '../utilities/user.decorator';
import { IUser } from '../types/user';

@Controller('likes')
export class LikesController {
  constructor(private readonly likesService: LikesService) {}

  @Post(':postId')
  @UseGuards(AuthGuard('jwt'))
  async addLike(@Param('postId') postId: string, @User() user: IUser) {
    return await this.likesService.add(postId, user._id);
  }

  @Delete(':postId')
  @UseGuards(AuthGuard('jwt'))
  async removeLike(@Param('postId') postId: string, @User() user: IUser) {
    return await this.likesService.remove(postId, user._id);
  }
}
