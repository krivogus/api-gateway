import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ILikes } from '../types/likes';
import { Model } from 'mongoose';

@Injectable()
export class LikesService {
  constructor(
    @InjectModel('Likes') private readonly likesModel: Model<ILikes>,
  ) {}

  async add(postId: string, userId: string) {
    const likes = await this.likesModel.findOneAndUpdate(
      { post: postId },
      { $addToSet: { users: userId } },
      { new: true },
    );

    return await this.likesModel.findOneAndUpdate(
      { post: postId },
      { $set: { amount: likes.users.length } },
      { new: true },
    );
  }

  async remove(postId: string, userId: string) {
    const likes = await this.likesModel.findOneAndUpdate(
      { post: postId },
      { $pull: { users: userId } },
      { new: true },
    );

    return await this.likesModel.findOneAndUpdate(
      { post: postId },
      { $set: { amount: likes.users.length } },
      { new: true },
    );
  }
}
