import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { SharedModule } from './shared/shared.module';
import { AuthModule } from './auth/auth.module';
import { PostService } from './post/post.service';
import { PostModule } from './post/post.module';
import { LikesModule } from './likes/likes.module';
import { CommentController } from './comment/comment.controller';
import { CommentService } from './comment/comment.service';
import { CommentModule } from './comment/comment.module';
import { FriendService } from './friend/friend.service';
import { FriendModule } from './friend/friend.module';
import { FileStorageModule } from './file-storage/file-storage.module';
import { ProfileModule } from './profile/profile.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    MongooseModule.forRoot(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useFindAndModify: false,
    }),
    SharedModule,
    AuthModule,
    PostModule,
    LikesModule,
    CommentModule,
    FriendModule,
    FileStorageModule,
    ProfileModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
