import { Controller, Put, UseGuards, Param, Delete } from '@nestjs/common';
import { FriendService } from './friend.service';
import { AuthGuard } from '@nestjs/passport';
import { User } from 'src/utilities/user.decorator';
import { IUser } from 'src/types/user';

@Controller('friend')
export class FriendController {
  constructor(private readonly friendService: FriendService) {}

  @Put(':friendId')
  @UseGuards(AuthGuard('jwt'))
  async addFriend(@Param('friendId') friendId: string, @User() user: IUser) {
    return await this.friendService.add(friendId, user._id);
  }

  @Delete(':friendId')
  @UseGuards(AuthGuard('jwt'))
  async removeFriend(@Param('friendId') friendId: string, @User() user: IUser) {
    return await this.friendService.remove(friendId, user._id);
  }
}
