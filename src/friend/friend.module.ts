import { Module } from '@nestjs/common';
import { FriendController } from './friend.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { UserSchema } from 'src/models/user.schema';
import { FriendService } from './friend.service';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])],
  controllers: [FriendController],
  providers: [FriendService],
})
export class FriendModule {}
