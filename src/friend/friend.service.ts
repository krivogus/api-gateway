import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from '../types/user';

@Injectable()
export class FriendService {
  constructor(@InjectModel('User') private readonly userModel: Model<IUser>) {}

  async add(friendId: string, userId: string) {
    return await this.userModel.findByIdAndUpdate(
      userId,
      { $addToSet: { friends: friendId } },
      { new: true },
    );
  }

  async remove(friendId: string, userId: string) {
    return await this.userModel.findByIdAndUpdate(
      userId,
      { $pull: { friends: friendId } },
      { new: true },
    );
  }
}
