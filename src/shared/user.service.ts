import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { Model } from 'mongoose';
import { IUser } from '../types/user';
import { InjectModel } from '@nestjs/mongoose';
import { IRegisterDTO, ILoginDTO } from '../auth/auth.dto';
import * as bcrypt from 'bcrypt';
import { IProfile } from '../types/profile';

@Injectable()
export class UserService {
  constructor(
    @InjectModel('User') private userModel: Model<IUser>,
    @InjectModel('Profile') private profileModel: Model<IProfile>,
  ) {}

  private sanitizeUser(user: IUser) {
    const sanitized = user.toObject();
    const { password, __v, ...updatedUserData } = sanitized;
    return updatedUserData;
  }

  async create(userDTO: IRegisterDTO) {
    const { username } = userDTO;
    const user = await this.userModel.findOne({ username });
    if (user) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const createdUser = new this.userModel(userDTO);
    const createdProfile = await new this.profileModel({
      user: createdUser._id,
    });
    createdUser.profile = createdProfile._id;
    await Promise.all([createdUser.save(), createdProfile.save()]);

    const populated = await createdUser
      .populate({ path: 'profile' })
      .execPopulate();

    return this.sanitizeUser(populated);
  }

  async findByLogin(userDTO: ILoginDTO) {
    const { username, password } = userDTO;
    const user = await this.userModel.findOne({ username }).select('+password');

    if (!user) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }

    if (await bcrypt.compare(password, user.password)) {
      return this.sanitizeUser(user);
    } else {
      throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
    }
  }

  async findByPayload(payload: any) {
    const { username } = payload;
    return await this.userModel.findOne({ username });
  }
}
