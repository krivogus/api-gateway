import { Document } from 'mongoose';
import { IPost } from './post';
import { ILikes } from './likes';
import { IProfile } from './profile';

interface IAddress {
  city: string;
  country: string;
}

export interface IUser extends Document {
  profile: IProfile | string;
  username: string;
  password: string;
  createdAt: Date;
  firstName: string;
  lastName: string;
  friends: [IUser | string];
  posts: [IPost | string];
  address?: IAddress | string;
  status?: string;
  likes: ILikes | string;
}
