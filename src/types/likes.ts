import { Document } from 'mongoose';
import { IUser } from './user';
import { IPost } from './post';

export interface ILikes extends Document {
  createdAt: Date;
  users: [IUser | string];
  amount: number;
  post: IPost | string;
}
