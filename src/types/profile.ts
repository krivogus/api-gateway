import { IUser } from './user';
import { Document } from 'mongoose';

export interface IProfile extends Document {
  user: IUser | string;
  avatarSrc: string;
}
