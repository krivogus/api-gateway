declare global {
  namespace NodeJS {
    interface ProcessEnv {
      MONGO_URI: string;
      MONGO_URI_TEST: string;
      SECRET_KEY: string;
      PORT: string;
      GLOBAL_PREFIX: string;
      NODE_ENV: 'development' | 'test' | 'production';
    }
  }
}

export {};
