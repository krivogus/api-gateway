import { Document } from 'mongoose';
import { IComment } from './comment';
import { ILikes } from './likes';
import { IUser } from './user';

export interface IPost extends Document {
  createdAt: Date;
  data: {
    text?: string;
    photo?: string;
  };
  author: IUser | string;
  comments?: [IComment | string];
  likes?: ILikes | string;
}
