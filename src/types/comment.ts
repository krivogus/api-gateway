import { Document } from 'mongoose';
import { IUser } from './user';
import { IPost } from './post';

export interface IComment extends Document {
  post: IPost | string;
  createdAt: Date | string;
  author: IUser | string;
  data: {
    text: string;
  };
}
